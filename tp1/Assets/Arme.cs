using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arme : MonoBehaviour
{
    public GameObject bullet;
    public Transform bullet_start;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        // pour tirer
        if(Input.GetKeyDown(KeyCode.X)) 
        {
            // Instancie une balle
            Instantiate(bullet, bullet_start.transform.position,
                bullet_start.transform.rotation);
        }   
    }
}
