using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera_Manager : MonoBehaviour
{
    // Camera qui ne bouge pas
    public Camera view_camera;
    // Camera autour du personnage
    public Camera third_person_camera;

    // active la camera view_camera et desactive third_person_camera
    private void set_view_camera()
    {
        view_camera.enabled = true;
        third_person_camera.enabled = false;
        view_camera.GetComponent<AudioListener>().enabled = true;
        third_person_camera.GetComponent<AudioListener>().enabled = false;
    }
    private void set_third_person_camera()
    {
        view_camera.enabled = false;
        third_person_camera.enabled = true;
        view_camera.GetComponent<AudioListener>().enabled = false;
        third_person_camera.GetComponent<AudioListener>().enabled = true;
    }

    // switch les cameras active
    private void switch_camera()
    {
        if (view_camera.enabled)
            set_third_person_camera();
        else 
            set_view_camera();
    }

    // Start is called before the first frame update
    void Start()
    {
        set_view_camera();
    }

    // Update is called once per frame
    void Update()
    {
        // Change la camera en cours apres k
        if (Input.GetKeyDown(KeyCode.K))
            switch_camera();
    }
}
