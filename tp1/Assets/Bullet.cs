using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    // vitesse de la balle
    public float bullet_speed = 10;
    // la balle est detruit apres x de time
    public float destroy_time = 5;
    // Start is called before the first frame update
    void Start()
    {
        // Pour qu'elle se detruise quand elle est trop loin
        Destroy(gameObject, destroy_time);
    }

    void Update()
    {
        transform.Translate(bullet_speed * Time.deltaTime * Vector3.forward);
    }
}
