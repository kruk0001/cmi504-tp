using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mouvement : MonoBehaviour
{
    // vitesse d'acceleration
    public float speed = 2;
    // vitesse de base
    public float base_speed = 0.07f;
    // rotation de base
    public float rotate_speed = 2f;
    // hauteur du saut
    public float jump_speed = 2f;
    private Rigidbody rb;
    // frame consecutive au meme Y pour refaire un jump
    public int CONSICUTIVE_FRAME_REQUIRED = 3;
    private int consicutive_frame = 0;

    private float last_y;

    // Appeler quand jump est presse   
    private void Jump()
    {
        if (last_y == transform.position.y) // au sol
        {
            if (consicutive_frame >= CONSICUTIVE_FRAME_REQUIRED)
            {
                consicutive_frame = 0;
                rb.AddForce(Vector3.up * jump_speed, ForceMode.Impulse);
            }
            else consicutive_frame++;
        }
        else consicutive_frame = 0;
        last_y = transform.position.y;
    }

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        last_y = transform.position.y;
    }

    // Update is called once per frame
    void Update()
    {
        float new_speed = 1;

        if (Input.GetKey(KeyCode.LeftShift))
        {
            new_speed = speed; // pour accelerer
        }

        if (Input.GetKey(KeyCode.Z))
        {
            transform.Translate(base_speed * new_speed * Time.deltaTime * Vector3.forward);
        }
        if (Input.GetKey(KeyCode.S))
        {
            transform.Translate(base_speed * new_speed * Time.deltaTime * Vector3.back);
        }
        if (Input.GetKey(KeyCode.D))
        {
            transform.Translate(base_speed * new_speed * Time.deltaTime * Vector3.right);
        }
        if (Input.GetKey(KeyCode.Q))
        {
            transform.Translate(base_speed * new_speed * Time.deltaTime * Vector3.left);
        }
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            transform.Rotate(Vector3.up, -rotate_speed * Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.RightArrow))
        {
            transform.Rotate(Vector3.up, rotate_speed * Time.deltaTime);
        }
    }

    // est appeler a un temps constant independamment des fps du user
    void FixedUpdate()
    {
        if(Input.GetKey(KeyCode.Space)) 
        {
            Jump();
        }
    }
}
