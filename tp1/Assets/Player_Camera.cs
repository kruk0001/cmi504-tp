using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_Camera : MonoBehaviour
{
    // Vitesse de rotation
    public float rotate_speed = 2f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        // Touche pour changer la rotation de la camera
        if (Input.GetKey(KeyCode.UpArrow))
        {
            transform.Rotate(Vector3.left, rotate_speed * Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.DownArrow))
        {
            transform.Rotate(Vector3.right, rotate_speed * Time.deltaTime);
        }
    }
}
