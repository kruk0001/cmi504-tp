using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RouesMouvement : MonoBehaviour
{
    // vitesse d'acceleration
    public float speed = 2;
    // vitesse de base
    public float base_speed = 100;
    // rotation de base
    public float rotate_speed = 2f;

    private Rigidbody rb;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        float new_speed = 1;

        if (Input.GetKey(KeyCode.LeftShift))
        {
            new_speed = speed; // pour accelerer
        }

        if (Input.GetKey(KeyCode.UpArrow))
        {
            rb.AddForce(base_speed * new_speed * transform.up, ForceMode.Acceleration);
        }
        if (Input.GetKey(KeyCode.DownArrow))
        {
            rb.AddForce(base_speed * new_speed * -transform.up, ForceMode.Acceleration);
        }

        if (Input.GetKey(KeyCode.LeftArrow))
        {
            rb.AddTorque(Vector3.down * rotate_speed, ForceMode.Force);
        }
        if (Input.GetKey(KeyCode.RightArrow))
        {
            rb.AddTorque(Vector3.up * rotate_speed, ForceMode.Force);
        }
    }
}
