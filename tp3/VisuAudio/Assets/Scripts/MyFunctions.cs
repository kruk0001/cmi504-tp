using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyFunctions : MonoBehaviour
{
    // pour mettre du rouge une fois sur deux 
    private bool isRed = false;

    // contient les objets que l'on instencie pour la barre de son
    private GameObject[] gameObjects;
    // modele des cubes pour la barre de son
    public GameObject modelsSpectrume;

    public void Start()
    {
        gameObjects = new GameObject[12];

        // boucle pour instancier 12 barres de son. 12 car spectrum.Length = 12
        for (int i= 0; i < gameObjects.Length; i++)
        {
            gameObjects[i] = Instantiate(modelsSpectrume, transform);
            gameObjects[i].transform.position = transform.position + 
                (Vector3.left * i);
        }
    }

    // function a mettre dans les listeners de OnBeat de Audiovis
    public void onOnbeatDetected()
    {
        if(isRed)
            // On met en noir
            GetComponent<Renderer>().material.color = Color.black;
        else 
            // on met en rouge
            GetComponent<Renderer>().material.color = Color.red;
        isRed = !isRed;
    }

    // function a mettre dans les listeners de OnSpectrum de Audiovis
    public void onSpectrum(float[] spectrum)
    {
        for(int i = 0; i < spectrum.Length; i++) 
        {
            // securite au cas ou la taille change
            if(i >= gameObjects.Length)
            {
                return;
            }

            // on change le scale en z en fonction du spectre [i]
            gameObjects[i].transform.localScale = new Vector3(
                gameObjects[i].transform.localScale.x,
                gameObjects[i].transform.localScale.y,
                spectrum[i] * 100);
        }
    }
}
