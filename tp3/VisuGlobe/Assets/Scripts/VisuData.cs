using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VisuData : MonoBehaviour
{
    public Transform cible;
    public GameObject visuCube;
    public float multiplier;

    Vector3 latloncart(float lat, float lon)
    {
        Vector3 pos;

        float x = 0.5f * Mathf.Cos(lon) * Mathf.Cos(lat);
        float y = 0.5f * Mathf.Cos(lon) * Mathf.Sin(lat);
        float z = 0.5f * Mathf.Sin(lon);

        pos.x = 0.5f * Mathf.Cos((lon) * Mathf.Deg2Rad) * Mathf.Cos(lat * Mathf.Deg2Rad); 
        pos.y = 0.5f * Mathf.Sin(lat * Mathf.Deg2Rad);
        pos.z = 0.5f * Mathf.Sin((lon) * Mathf.Deg2Rad) * Mathf.Cos(lat * Mathf.Deg2Rad);

        return pos;
    }

    public void visualCube(float lat, float lon, float val, float multiplier)
    {
        // j'ai cree un shader qui permet de definir une couleur grandiante 
        // entre deux couleurs predifinis.
        // le shader prend en parametre une valeur entre 0 et 1

        GameObject cube = GameObject.Instantiate(visuCube);
       
        Vector3 pos;

        pos = latloncart(lat, lon);
        cube.transform.position = new Vector3(pos.x, pos.y, pos.z);
        cube.transform.LookAt(cible, Vector3.back);

        Vector3 echelle = cube.transform.localScale;
        echelle.z = val * multiplier;
        cube.transform.localScale = echelle;

        // on obtient le shader que j cree et que j mis sur la prefabe du cube 
        // et on fait passer le parametre _GradientValue qui est entre 0 et 1
        // qui permet de mettre la bonne couleur gradiante
        //cube.transform.GetChild(0).GetComponent<Renderer>().material.SetFloat("_GradientValue",
        //  cube.transform.position.z);
        cube.transform.GetChild(0).GetComponent<Renderer>().material.SetFloat("_GradientValue",
          val / 20000000);
    }
}
