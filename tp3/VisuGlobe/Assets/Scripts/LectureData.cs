using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LectureData : MonoBehaviour
{
    public TextAsset jsonFile;
    public VisuData visualize;

    // Start is called before the first frame update
    void Start()
    {
        Cities employeesInJson = JsonUtility.FromJson<Cities>(jsonFile.text); 

        foreach (City cities in employeesInJson.cities)
        {
            visualize.visualCube(
                cities.lat,
                cities.lng,
                cities.population,
                visualize.multiplier
                );
        }
    }
}
