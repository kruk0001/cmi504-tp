using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraOrbite : MonoBehaviour
{
    public GameObject cible;

    public float anglesParSeconde = 45;

    // Start is called before the first frame update
    void Start()
    {
        // met la camera autour de l'objet cible
        transform.position = new Vector3(2, 0, 0);
        transform.LookAt(cible.transform.position);
    }

    // Update is called once per frame
    void Update()
    {
        // Permet d'avoir la direction des touches fleches gauches / fleches droites
        float horizontalInput = Input.GetAxis("Horizontal");

        // permet d'avoir le veteur (0, 1, 0) ou (0, -1, 0)
        Vector3 direction = new Vector3(0, -horizontalInput, 0);
        // avoir le vecteur de rotation
        Vector3 rotation = direction * anglesParSeconde * Time.deltaTime;

        // on evite de update avec un vecteur null
        if (rotation != Vector3.zero)
        {
            // update la rotation
            transform.RotateAround(cible.transform.position, rotation, 
                anglesParSeconde * Time.deltaTime);
        }
    }
}
